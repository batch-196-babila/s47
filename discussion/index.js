// Show posts
const showPosts = (posts) => {
    let postEntries = '';
    posts.forEach((post) => {
        // /console.log(post);
        postEntries += `
            <div id="post-${post.id}">
                <h3 id="post-title-${post.id}">${post.title}</h3>
                <p id="post-body-${post.id}">${post.body}</p>
                <button onclick="editPost('${post.id}')">Edit</button>
                <button onclick="deletePost('${post.id}')">Delete</button>
            </div>
        `;
    })
    document.querySelector('#div-post-entries').innerHTML = postEntries;
};
/*
    Syntax: fetch(url, options)

*/
// chain method sample: .then .catch

fetch('https://jsonplaceholder.typicode.com/posts').then(response => response.json()).then(data => showPosts(data));

// Add post
document.querySelector('#form-add-post').addEventListener('submit', (e) => {
    e.preventDefault();
    fetch('https://jsonplaceholder.typicode.com/posts', {
        method: 'POST', // all caps
        body: JSON.stringify({
            title: document.querySelector("#txt-title").value,
            body: document.querySelector("#txt-body").value,
            userId: 1
        }),
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            alert('Post added!');
            document.querySelector('#txt-title').value = null;
            document.querySelector('#txt-body').value = null;
        })
})

// Edit Post
const editPost = (id) => {
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector(`#txt-edit-id`).value = id;
    document.querySelector(`#txt-edit-title`).value = title;
    document.querySelector(`#txt-edit-body`).value = body;

    document.querySelector("#btn-submit-update").removeAttribute('disabled');
};

// Update Post

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
    e.preventDefault();
    fetch('https://jsonplaceholder.typicode.com/posts/1', {
        method: 'PUT',
        body: JSON.stringify({
            id: document.querySelector("#txt-edit-id"),
            title: document.querySelector("#txt-edit-title").value,
            body: document.querySelector("#txt-edit-body").value,
            userId: 1
        }),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(data => {
        console.log(data)
        alert("Post updated!");

        document.querySelector("#txt-edit-id").value = null;
        document.querySelector("#txt-edit-title").value = null;
        document.querySelector("#txt-edit-body").value = null;
        document.querySelector("#btn-submit-update").disabled = true;
        // document.querySelector("#btn-submit-update").setAttribute('disabled', true);
    })
})

// ACTIVITY SOL'N
const deletePost = (id) => {
    fetch('https://jsonplaceholder.typicode.com/posts/1', {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(deleteResponse => deleteResponse.json())
    .then(deleteData => {
        console.log(deleteData);
        let deleteDiv = document.querySelector(`#post-${id}`);
        console.log(deleteDiv);
        deleteDiv.remove();
    })
}

